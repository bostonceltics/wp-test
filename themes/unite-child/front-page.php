<?php
/**
 * Template Name: Главная
 */
?>
<?php

if ( get_option( 'show_on_front' ) == 'posts' ) {
    get_template_part( 'index' );
} elseif ( 'page' == get_option( 'show_on_front' ) ) {

get_header(); ?>

<div id="primary" class="content-area col-sm-12 col-md-12">
    <main id="main" class="site-main" role="main">
        <div class="col-sm-12 col-md-9">
            <div class="row">
                <!-- GET OBJECTS START-->
                <?php $query = new WP_Query( array( 'post_type' => 'agency', 'posts_per_page' => 1, 'post__in' => array($_GET['agency_id'])));
                if ( $query->have_posts() ):
                    while ( $query->have_posts() ) : $query->the_post();
                        $featured_posts = get_field('agency_objects');
                        if( $featured_posts ): 
                            foreach( $featured_posts as $post ): 
                                setup_postdata($post);
                                get_template_part('templates/item');
                            endforeach; 
                            wp_reset_postdata(); 
                        endif;
                    endwhile; wp_reset_postdata(); 
                else:
                    $cache_key = 'real-estate-query';
                    if ( !$query = get_transient( $cache_key ) ) {
                        $query = new WP_Query( array( 'post_type' => 'real-estate','posts_per_page' => -1));
                        set_transient( $cache_key, $query, 24 * HOUR_IN_SECONDS );
                    }
                    if ( $query->have_posts() ) :
                        while ( $query->have_posts() ) : $query->the_post(); 
                            get_template_part('templates/item');
                        endwhile; wp_reset_postdata();
                    endif;
                endif;?>
                <!-- GET OBJECTS END-->  
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <?php get_template_part('templates/sidebar');?>
        </div>
    </main>
</div>

<?php
get_footer();
}
?>