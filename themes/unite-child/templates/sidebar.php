<div class="list-group">
<a href="javascript:void(0);" class="list-group-item list-group-item-action active">АГЕНСТВА</a>
<?php $query = new WP_Query( array( 'post_type' => 'agency','posts_per_page' => -1));
if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post(); ?>
    <form action="/" method="GET" class="filter_item">
        <input type="text" name="agency_id" value="<?=get_the_ID();?>" hidden>
        <button class="list-group-item list-group-item-action"><?php the_title();?></button>
    </form>
    <?php endwhile; wp_reset_postdata();
endif;?>
<a href="/" class="list-group-item list-group-item-action">Все объекты</a>
</div>