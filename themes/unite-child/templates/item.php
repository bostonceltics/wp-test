<div class="col-sm-12 col-md-4">
    <article class="wp-caption alignnone thumbnail">
        <img loading="lazy" class="wp-image-59" alt="" src="<?=get_the_post_thumbnail_url();?>">    
        <h5><?php the_title();?></h5>
        <p>Тип: <?php $term_obj_list = get_the_terms( get_the_ID(), 'type-real-estate' ); echo $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));?></p>
        <p>Площадь: <?=get_field('real_estate_square');?> м<sup>2</sup></p>
        <p>Стоимость: <?=get_field('real_estate_cost');?>$</p>
        <p>Адрес: <?=get_field('real_estate_adres');?></p>
        <p>Жилая площадь: <?=get_field('real_estate_live_square');?> м<sup>2</sup></p>
        <p>Этаж: <?=get_field('real_estate_floor');?></p>
    </article>
</div>