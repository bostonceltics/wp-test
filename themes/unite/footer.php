<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package unite
 */
?>
            </div><!-- row -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<div class="row">
				<div class="copyright col-md-12" style="text-align:center">
					<?php do_action( 'unite_credits' ); ?>
					<?php echo unite_get_option( 'custom_footer_text' ); ?>
					<?php do_action( 'unite_footer' ); ?>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon --> 
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>